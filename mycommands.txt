# Download Serbia latest PBF
wget http://download.geofabrik.de/europe/serbia-latest.osm.pbf

# Upload to my account
az storage blob upload --account-name stambenezajednice --container-name "\$web" --file result.csv --name avgust2019.csv
az storage blob upload --account-name stambenezajednice --container-name "\$web" --file index.html --name avgust2019.html