# -*- coding: utf-8 -*-


"""
This file is only needed to generate OSM<->municipality mapping, needed for https://regio-osm.de/ project.
"""
import overpy
import csv
from common import read_output_csv, osm_to_dataset_municipality

api = overpy.Overpass(url='http://overpass.openstreetmap.fr/api/interpreter')


def write_csv(output_file, output):
    with open(output_file, 'w') as h:
        writer = csv.DictWriter(h, fieldnames=['municipalityosm', 'municipalitynamedataset',
                                               'wikidata', 'osmadminlevel', 'hierarchy'])
        writer.writeheader()
        for data in output:
            writer.writerow(data)


def find_municipalities_ovepass():
    result = api.query("""
area["name"="Србија"]["admin_level"=2]->.a;
(
    relation(area.a)["admin_level"="7"];
    relation(area.a)["admin_level"="8"];
);
out;
// &contact=branko@kokanovic.org
    """)
    return [
        {'name': r.tags['name'],
         'admin_level': r.tags['admin_level'],
         'wikidata': r.tags['wikidata'] if 'wikidata' in r.tags else ''
         }
        for r in result.relations]


def main():
    output = []
    stambene_zajednice = read_output_csv('result.csv')
    # extract distinct municipalities from catastre
    for sz in stambene_zajednice:
        found = False
        for data in output:
            if sz['municipality'] == data['municipalitynamedataset']:
                found = True
                break
        if not found:
            output_row = {}
            output_row['municipalitynamedataset'] = sz['municipality']
            output_row['municipalityosm'] = 'МАЛО ЦРНИЋЕ'
            output_row['wikidata'] = ''
            output_row['osmadminlevel'] = ''
            output_row['hierarchy'] = 'Србија,' + sz['district']
            output.append(output_row)
    # Now try to match with OSM data
    osm_municipalities = find_municipalities_ovepass()
    for data in output:
        found = -1
        for index, osm_municipality in enumerate(osm_municipalities):
            possible_matches = osm_to_dataset_municipality(osm_municipality['name'].lower())
            for possible_match in possible_matches:
                if possible_match == data['municipalitynamedataset'].lower():
                    data['municipalityosm'] = osm_municipality['name']
                    data['wikidata'] = osm_municipality['wikidata']
                    data['osmadminlevel'] = osm_municipality['admin_level']
                    found = index
                    break
            if found > -1:
                break
        if found > -1:
            del osm_municipalities[found]

    print('Remaining municipalities:', osm_municipalities)
    # Hack to get remaining municipalities that do not have buildings
    malo_crnice = next(m for m in osm_municipalities if m['wikidata'] == 'Q2047453')
    output.append({
        'municipalitynamedataset': 'МАЛО ЦРНИЋЕ',
        'municipalityosm': malo_crnice['name'],
        'wikidata': malo_crnice['wikidata'],
        'osmadminlevel': malo_crnice['admin_level'],
        'hierarchy': 'Србија,БРАНИЧЕВСКИ ОКРУГ'})
    presevo = next(m for m in osm_municipalities if m['wikidata'] == 'Q157363')
    output.append({
        'municipalitynamedataset': 'ПРЕШЕВО',
        'municipalityosm': presevo['name'],
        'wikidata': presevo['wikidata'],
        'osmadminlevel': presevo['admin_level'],
        'hierarchy': 'Србија,ПЧИЊСКИ ОКРУГ'})
    write_csv('regio-osm.csv', output)


if __name__ == '__main__':
    main()
