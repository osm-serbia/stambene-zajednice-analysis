# -*- coding: utf-8 -*-

from typing import List
import os
import csv
from operator import itemgetter


def read_input_csv(csvfile: str)->List:
    """
    Reads input CSV, as given by RGZ (Republicki Geodetski Zavod).
    Can be found somewhere over on https://data.gov.rs/sr/datasets/registar-stambenikh-zajednitsa-2/
    :param csvfile: Input CSV file
    :return: Read array
    """
    stambene_zajednice = []
    if not os.path.isfile('registar-stambenih-zajednica.csv'):
        raise Exception('Missing registar-stambenih-zajednica.csv, download it from https://data.gov.rs/sr/')

    with open(csvfile) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            stambene_zajednice.append(
                {'district': row['OkrugNaziv1'], 'municipality': row['OpstinaNaziv1'],
                 'street': row['Ulica'], 'number': row['KucniBroj'],
                 'processed': False})
    return stambene_zajednice


def read_output_csv(input_file: str, filter_district: str = None, filter_municipality: str = None,
                    max_row_to_read: int = -1) -> List:
    """
    Reads output CSV, and normalizes data. Output CSV is output of main.py file, which contains results of analysis.
    It is usually called output.csv or result.csv
    :param csvfile: CSV file to read
    :return: List of parsed and normalized data from CSV
    """
    stambene_zajednice = []
    with open(input_file) as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if len(stambene_zajednice) > max_row_to_read > 0:
                break
            if filter_district is not None and row['district'] != filter_district:
                continue
            if filter_municipality is not None and row['municipality'] != filter_municipality:
                continue
            stambene_zajednice.append(row)

    for sz in stambene_zajednice:
        sz['street'] = sz['street'].replace('"', '\\"')
        sz['node'] = [] if len(sz['node']) <= 2 else [int(i) for i in sz['node'][1:-1].split(',')]
        sz['way'] = [] if len(sz['way']) <= 2 else [int(i) for i in sz['way'][1:-1].split(',')]
        sz['relation'] = [] if len(sz['relation']) <= 2 else [int(i) for i in sz['relation'][1:-1].split(',')]
        sz['found'] = len(sz['node']) > 0  or len(sz['way']) > 0 or len(sz['relation']) > 0
        sz['multiple_entities_same_housenumber'] = sz['multiple_entities_same_housenumber'] or False
        sz['building_tag_present'] = sz['building_tag_present'] or False
        sz['building_is_apartments'] = sz['building_is_apartments'] or False
    stambene_zajednice.sort(key=itemgetter('district', 'municipality', 'street', 'number'))
    return stambene_zajednice


def osm_to_dataset_municipality(municipality: str) -> List[str]:
    """
    Try to convert name from OSM municipality name to name from catastre
    :param municipality: Name of municipality, as from OSM
    :return: Name from municipality, as if from catastre
    """
    if municipality.startswith('градска општина '):
        trimmed = municipality.replace('градска општина ', '')
        if trimmed == 'палилула':
            return [trimmed, 'палилула (београд)']
        else:
            return [trimmed]
    if municipality.startswith('општина '):
        trimmed = municipality.replace('општина ', '')
        if trimmed == 'палилула':
            return [trimmed, 'палилула (ниш)']
        return [trimmed]
    if municipality.startswith('град '):
        return [municipality.replace('град ', '')]
    return [municipality, ]