# -*- coding: utf-8 -*-

import csv
import logging
import logging.handlers
import os.path
import pickle
import random
import socket
import urllib.error
import argparse
from time import sleep
from typing import List

import osmium
import overpy
from geopy.exc import GeocoderServiceError
from geopy.geocoders import Nominatim
from transliteration import cyr2lat

from common import read_input_csv, read_output_csv, osm_to_dataset_municipality

api = overpy.Overpass(url='http://overpass.openstreetmap.fr/api/interpreter')
geolocator = Nominatim(user_agent="https://gitlab.com/stalker314314/stambene-zajednice-analysis")


class BailoutEarlyException(Exception):
    pass


def setup_logger(filename: str):
    """Simple logger used throughout whole scrapper - logs both to file and console."""
    logging_level = logging.INFO
    l = logging.getLogger()
    l.setLevel(logging_level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    ch = logging.handlers.TimedRotatingFileHandler(filename=filename, when='midnight', interval=1)
    ch.setLevel(logging_level)
    ch.setFormatter(formatter)
    l.addHandler(ch)

    ch = logging.StreamHandler()
    ch.setLevel(logging_level)
    ch.setFormatter(formatter)
    l.addHandler(ch)

    return l


logger = setup_logger('stambene-jedinice-analysis.log')


def retry_on_error(timeout_in_seconds=5*60):
    def decorate(func):
        def call(*args, **kwargs):
            retries = 5
            while retries > 0:
                try:
                    result = func(*args, **kwargs)
                except (ConnectionRefusedError, GeocoderServiceError, socket.timeout, urllib.error.URLError):
                    retries = retries - 1
                    logger.warning('Connection refused, retrying')
                    sleep(timeout_in_seconds)
                    continue
                return result
            raise Exception('Exhausted retries for connection refused, quitting')
        return call
    return decorate


def normalize_house_numbers(house_number: str) -> str:
    return cyr2lat(house_number).upper()


class OsmHandler(osmium.SimpleHandler):
    def __init__(self, all_addresses):
        super(OsmHandler, self).__init__()
        self.all_addresses = all_addresses
        self.processed = 0

    def process_entity(self, entity, entity_type):
        # If needed, this is how we can stop execution early
        #if self.processed > 1000000:
        #    raise BailoutEarlyException()
        self.processed = self.processed + 1
        if 'addr:street' in entity.tags and 'addr:housenumber' in entity.tags:
            if entity.tags['addr:street'].upper() not in self.all_addresses:
                self.all_addresses[entity.tags['addr:street'].upper()] = []
            normalized_house_numbers = normalize_house_numbers(entity.tags['addr:housenumber'])
            self.all_addresses[entity.tags['addr:street'].upper()].append(
                {'number': normalized_house_numbers,
                 'osm_id': entity.id,
                 'osm_type': entity_type
                 }
            )

    def node(self, n):
        self.process_entity(n, 'node')

    def way(self, w):
        self.process_entity(w, 'way')

    def relation(self, r):
        self.process_entity(r, 'relation')


def osm_to_dataset_district(district: str) -> List[str]:
    if ' управни ' in district:
        return [district.replace('управни ', '')]
    return [district, ]


@retry_on_error()
def find_district(entity_id: str, entity_type: str):
    sleep(0.5)
    if entity_type == 'relation':
        result = api.query("""
rel({0});
way(r);
node(w);
is_in;
area._[admin_level="6"];
out;
// &contact=branko@kokanovic.org
                """.format(entity_id))
    elif entity_type == 'way':
        result = api.query("""
way({0});
node(w);
is_in;
area._[admin_level="6"];
out;
// &contact=branko@kokanovic.org
        """.format(entity_id))
    else:
        result = api.query("""
node({0});
is_in;
area._[admin_level="6"];
out;
// &contact=branko@kokanovic.org
                """.format(entity_id))

    if len(result.areas) == 0:
        # It got deleted in the meantime
        return None
    if len(result.areas) != 1:
        raise Exception("query found not one district for entity {0}{1}, but {2}".format(entity_type[0], entity_id, len(result.areas)))
    return result.areas[0].tags['name']


@retry_on_error()
def find_municipality(entity_id: str, entity_type: str):
    sleep(0.5)
    for level in [8, 7, 9]:
        if entity_type == 'relation':
            result = api.query("""
rel({0});
way(r);
node(w);
is_in;
area._[admin_level="{1}"];
out;
// &contact=branko@kokanovic.org
                            """.format(entity_id, level))
        elif entity_type == 'way':
            result = api.query("""
way({0});
node(w);
is_in;
area._[admin_level="{1}"];
out;
// &contact=branko@kokanovic.org
                """.format(entity_id, level))
        else:
            result = api.query("""
node({0});
is_in;
area._[admin_level="{1}"];
out;
// &contact=branko@kokanovic.org
                            """.format(entity_id, level))
        if len(result.areas) < 1:
            continue
        elif len(result.areas) == 1:
            return result.areas[0].tags['name']
        else:
            raise Exception('Found more than one municipality for entity {0}'.format(entity_id))

    raise Exception("Query found no municipality for entity {0}{1}, but more than 1".format(entity_type[0], entity_id))


@retry_on_error()
def get_node(node_id: str):
    result = api.query("""
node({0});
out;
// &contact=branko@kokanovic.org
                    """.format(node_id))
    if len(result.nodes) != 1:
        raise Exception('Number of found nodes is not 1 for node {0}'.format(node_id))
    return result.nodes[0]


@retry_on_error()
def get_way(way_id: str):
    result = api.query("""
way({0});
out;
// &contact=branko@kokanovic.org
                    """.format(way_id))
    if len(result.ways) != 1:
        raise Exception('Number of found ways is not 1 for way {0}'.format(way_id))
    return result.ways[0]


@retry_on_error()
def get_relation(relation_id: str):
    result = api.query("""
relation({0});
out;
// &contact=branko@kokanovic.org
                    """.format(relation_id))
    if len(result.relations) != 1:
        raise Exception('Number of found relations is not 1 for relation {0}'.format(relation_id))
    return result.relations[0]


@retry_on_error()
def find_by_street_and_number_nominatim(street: str, number: str)->List:
    sleep(5)
    found_locations = geolocator.geocode('{0} {1}'.format(street, number),
                                         addressdetails=True, exactly_one=False, country_codes='rs')
    if not found_locations:
        found_locations = []
    logger.debug('Found {0} locations from Nominatim'.format(len(found_locations)))
    locations_with_housenumber = [loc.raw for loc in found_locations
                                  if 'address' in loc.raw and 'house_number' in loc.raw['address']]
    logger.debug('Found {0} locations with house numbers'.format(len(locations_with_housenumber)))
    location_with_exact_housenumber = [loc for loc in locations_with_housenumber
                                       if loc['address']['house_number'].lower() == number.lower()]
    logger.debug('Found {0} locations with matching house numbers'.format(len(location_with_exact_housenumber)))
    return location_with_exact_housenumber


@retry_on_error()
def find_by_street_and_number_ovepass(street: str, number: str):
    sleep(1)
    street = street.replace('"', '\\"')
    if number.isdigit():  # small optimization, no need for regex and case sensitivity, speed up query 2x
        housenumber_query = '["addr:housenumber"="{0}"]'.format(number)
    else:
        housenumber_query = '["addr:housenumber"~"^{0}$",i]'.format(number)
    result = api.query("""
(
    node["addr:street"~"^{0}$", i]{1}(42.2, 19.0, 46.2, 23.0);
    way["addr:street"~"^{0}$", i]{1}(42.2, 19.0, 46.2, 23.0);
);
out;
// &contact=branko@kokanovic.org
    """.format(street, housenumber_query))
    return result


def process_stambena_zajednica_with_overpass(sz: dict):
    """
    Deprecated. Sporo i netacno
    """
    results = find_by_street_and_number_ovepass(sz['street'], sz['number'])
    logger.info('Found {0} entities'.format(len(results.nodes + results.ways)))
    found_match = False
    found_district_municipality = []
    for entity in (results.nodes + results.ways):
        entity_type = 'way' if type(entity) == overpy.Way else 'node'
        associated_district = osm_to_dataset_district(find_district(entity, entity_type).lower())
        if associated_district and associated_district != sz['district'].lower():
            found_district_municipality.append('"{0}"'.format(associated_district))
            continue
        associated_municipality = osm_to_dataset_municipality(find_municipality(entity.id, entity_type).lower())
        if associated_municipality and associated_municipality != sz['municipality'].lower():
            found_district_municipality.append('"{0}"/"{1}"'.format(associated_district, associated_municipality))
            continue
        logger.info('Found match with entity {0}'.format(entity.id))
        if type(entity) == overpy.Way:
            sz['node'] = []
            sz['way'] = entity.id
            sz['relation'] = []
        else:
            sz['node'] = entity.id
            sz['way'] = []
            sz['relation'] = []
        sz['building_tag_present'] = 'building' in entity.tags
        sz['building_is_apartments'] = 'building' in entity.tags and entity.tags['building'] == 'apartments'
        sz['multiple_entities_same_housenumber'] = found_match
        if found_match:  # second find
            break
        found_match = True
    if not found_match and len(results.nodes + results.ways) > 0:
        logger.info('Not found any district/municipality for {0}:{1}, check if mapping is missing, we only found: {2}'
                    .format(sz['district'], sz['municipality'], ','.join([i for i in found_district_municipality])))

    sz['processed'] = True


def process_stambena_zajednica_with_nominatim(sz: dict):
    """
    Deprecated. Manje sporo od overpass-a, ali rezultati su cudni. Nekad Nominatim odluci da ne nadje ulicu i broj
    koji ocigledno postoje, a nekad je pametan i vrati node koji nema 'addr:street', ali je blizu te ulice. Sve u svemu,
    bas varira
    """
    # First try to get all nodes/way that match this street/number in it
    results = find_by_street_and_number_nominatim(sz['street'], sz['number'])
    found_match = False
    district_municipality_combinations = []
    sz['node'] = []
    sz['way'] = []
    sz['relation'] = []
    # Iterate for all results and see if any some street/number combination is in given district/municipality
    for entity in results:
        logger.debug('Examining entity {0}'.format(entity['address']))
        if entity['osm_type'] == 'relation':
            logger.warning('We found relation, this program do not support relations')
            continue
        # Find district and municipality from overpass turbo,
        # and then convert them from OSM name to name used in dataset (so we can match them)
        associated_counties = osm_to_dataset_district(find_district(entity['osm_id'], entity['osm_type']).lower())
        associated_cities = osm_to_dataset_municipality(find_municipality(entity['osm_id'], entity['osm_type']).lower())
        logger.debug('Found district/municipality {0}/{1}'.format(associated_counties, associated_cities))
        found_county_city_combination = False
        for county in associated_counties:
            for city in associated_cities:
                district_municipality_combinations.append((county.lower(), city.lower()))
                if (county, city) == (sz['district'].lower(), sz['municipality'].lower()):
                    found_county_city_combination = True
                    break
        if not found_county_city_combination:
            continue
        logger.info('Found match with entity {0}'.format(entity['osm_id']))
        # If we found a match, populate sz entry for it
        sz['multiple_entities_same_housenumber'] = found_match
        if entity['osm_type'] == 'node':
            sz['node'].append(entity['osm_id'])
            osm_entity = get_node(entity['osm_id'])
        elif entity['osm_type'] == 'way':
            sz['way'].append(entity['osm_id'])
            osm_entity = get_way(entity['osm_id'])
        else:
            sz['relation'].append(entity['osm_id'])
            osm_entity = get_relation(entity['osm_id'])
        building_found = 'building' in osm_entity.tags
        sz['building_tag_present'] = ('building_tag_present' in sz and sz['building_tag_present']) or building_found
        apartments_found = 'building' in osm_entity.tags and osm_entity.tags['building'] == 'apartments'
        sz['building_is_apartments'] = ('building_is_apartments' in sz and sz['building_is_apartments'])\
                                       or apartments_found
        logger.debug(sz)
        found_match = True
    if not found_match and len(results) > 0:
        found_combinations = ','.join(['"{0}"/"{1}"'.format(e[0], e[1]) for e in district_municipality_combinations])
        logger.info('Not found any district/municipality for {0}:{1}, check if mapping is missing, we only found: {2}'
                    .format(sz['district'].lower(), sz['municipality'].lower(), found_combinations))

    sz['processed'] = True


def process_stambena_zajednica_with_pbf(sz: dict, all_addresses: dict):
    # First try to get all nodes/way that match this street/number in it
    results = []
    if sz['street'].upper() in all_addresses:
        normalized_house_number = normalize_house_numbers(sz['number'])
        results = [e for e in all_addresses[sz['street'].upper()] if e['number'] == normalized_house_number]
    logger.debug('Found {0} entities: {1}'.format(
        len(results), ','.join(['{0}{1}'.format(e['osm_type'][0], e['osm_id']) for e in results])))
    found_match = False
    district_municipality_combinations = []
    sz['node'] = []
    sz['way'] = []
    sz['relation'] = []
    # Iterate for all results and see if any some street/number combination is in a given district/municipality
    for entity in results:
        logger.debug('Examining entity {0}{1}'.format(entity['osm_type'][0], entity['osm_id']))
        # Find district and municipality from overpass turbo,
        # and then convert them from OSM name to name used in dataset (so we can match them)
        district_res = find_district(entity['osm_id'], entity['osm_type'])
        if district_res is None:
            continue
        associated_counties = osm_to_dataset_district(district_res.lower())
        associated_cities = osm_to_dataset_municipality(find_municipality(entity['osm_id'], entity['osm_type']).lower())
        logger.debug('Found district/municipality {0}/{1}'.format(associated_counties, associated_cities))
        found_county_city_combination = False
        for county in associated_counties:
            for city in associated_cities:
                district_municipality_combinations.append((county.lower(), city.lower()))
                if (county, city) == (sz['district'].lower(), sz['municipality'].lower()):
                    found_county_city_combination = True
                    break
        if not found_county_city_combination:
            continue
        logger.info('Found match with entity {0}'.format(entity['osm_id']))
        # If we found a match, populate sz entry for it
        sz['multiple_entities_same_housenumber'] = found_match
        if entity['osm_type'] == 'node':
            sz['node'].append(entity['osm_id'])
            osm_entity = get_node(entity['osm_id'])
        elif entity['osm_type'] == 'way':
            sz['way'].append(entity['osm_id'])
            osm_entity = get_way(entity['osm_id'])
        else:
            sz['relation'].append(entity['osm_id'])
            osm_entity = get_relation(entity['osm_id'])
        building_found = 'building' in osm_entity.tags
        sz['building_tag_present'] = ('building_tag_present' in sz and sz['building_tag_present']) or building_found
        apartments_found = 'building' in osm_entity.tags and osm_entity.tags['building'] == 'apartments'
        sz['building_is_apartments'] = ('building_is_apartments' in sz and sz['building_is_apartments'])\
                                       or apartments_found
        logger.debug(sz)
        found_match = True
    if not found_match:
        if len(results) > 0:
            found_combinations = ','.join(['"{0}"/"{1}"'.format(e[0], e[1]) for e in district_municipality_combinations])
            logger.info('Not found any district/municipality for {0}:{1}, check if mapping is missing, we only found: {2}'
                        .format(sz['district'].lower(), sz['municipality'].lower(), found_combinations))
        else:
            logger.info('No match found for address {0}:{1}:{2}:{3}'.format(
                sz['district'], sz['municipality'], sz['street'], sz['number']))
    else:
        logger.info('Found {0} addresses ({1} nodes and {2} ways) matching address {3}:{4}:{5}:{6}'.format(
            len(results), 1 if sz['node'] else 0, 1 if sz['way'] else 0,
            sz['district'], sz['municipality'], sz['street'], sz['number']))
    sz['processed'] = True


def write_csv(output_file, stambene_zajednice: List[dict]):
    with open(output_file, 'w') as h:
        writer = csv.DictWriter(h, fieldnames=['district', 'municipality', 'street', 'number', 'processed',
                                               'node', 'way', 'relation', 'multiple_entities_same_housenumber',
                                               'building_tag_present', 'building_is_apartments'])
        writer.writeheader()
        for data in stambene_zajednice:
            writer.writerow(data)


def main():
    parser = argparse.ArgumentParser(
        description='Stambene zajednice analysis - helper tool to merge government dataset and OSM data')
    parser.add_argument('-o', '--output-file', default='output.csv',
                        help='Name of output CSV file. Default value is "output.csv"')
    parser.add_argument('-p', '--progress-file', default='progress.pickle',
                        help='Name of intermediate file where progress is saved. Default value is "progress.pickle"')
    parser.add_argument('--filter-district', default=None, help='If filtering by district should be used')
    parser.add_argument('--filter-municipality', default=None, help='If filtering by municipality should be used')
    parser.add_argument('--recover-progress-from-csv', action='store_true',
                        help='If we have CSV and we don\'t have progress, recover progress from CSV')
    args = parser.parse_args()
    progress_file = args.progress_file
    output_file = args.output_file
    filter_district = args.filter_district
    filter_municipality = args.filter_municipality
    recover_progress_from_csv = args.recover_progress_from_csv

    logger.info('Using {} as progress file'.format(progress_file))
    logger.info('Using {} as output file'.format(output_file))
    if filter_district:
        logger.info('Using {} for filtering districts'.format(filter_district))
    if filter_municipality:
        logger.info('Using {} for filtering municipalities'.format(filter_municipality))

    if not os.path.isfile('serbia-latest.osm.pbf'):
        logger.error('Download serbia-latest.osm.pbf from download.geofabrik.de to working directory and try again')
        return

    all_addresses = {}
    logger.info('Loading PBF, this can take some time')
    handler = OsmHandler(all_addresses)
    try:
        handler.apply_file('serbia-latest.osm.pbf')
    except BailoutEarlyException:
        pass
    logger.info('Loaded PBF')

    # Try to start from pickled state, if it exists. If it doesn't, try to load from output_file (result.csv).
    # If none exist, just load from CSV.
    if recover_progress_from_csv and not os.path.isfile(progress_file) and os.path.isfile(output_file):
        # This is case where once I accidentally deleted progress.pickle and had to regenerate state
        # from output_file (result.csv), so I kept the code, but this path should not be used.
        stambene_zajednice = read_input_csv('registar-stambenih-zajednica.csv')
        stambene_zajednice_output = read_output_csv(output_file)
        i = 0
        for sz_output in stambene_zajednice_output:
            logger.info('Processing row {0}'.format(i))
            i += 1
            if not sz_output['processed']:
                continue
            for sz in stambene_zajednice:
                if sz['district'] == sz_output['district'] and \
                        sz['municipality'] == sz_output['municipality'] and \
                        sz['street'] == sz_output['street'] and \
                        sz['number'] == sz_output['number']:
                    sz['processed'] = sz_output['processed']
                    sz['node'] = sz_output['node']
                    sz['way'] = sz_output['way']
                    sz['relation'] = sz_output['relation']
                    sz['multiple_entities_same_housenumber'] = sz_output['multiple_entities_same_housenumber']
                    sz['building_tag_present'] = sz_output['building_tag_present']
                    sz['building_is_apartments'] = sz_output['building_is_apartments']
                    break
        with open(progress_file, 'wb') as h:
            pickle.dump(stambene_zajednice, h, protocol=pickle.DEFAULT_PROTOCOL)
            sleep(5)

    if os.path.isfile(progress_file):
        with open(progress_file, 'rb') as h:
            stambene_zajednice = pickle.load(h)
    else:
        stambene_zajednice = read_input_csv('registar-stambenih-zajednica.csv')

    processed = 0
    random.shuffle(stambene_zajednice)  # shuffle to easily detect error from whole CSV
    for sz in stambene_zajednice:
        processed += 1
        if filter_district is not None and sz['district'] != filter_district:
            # logger.debug('Skipping {0}:{1}:{2}:{3} ({4}/{5}), filtered by district'.format(
            #     sz['district'], sz['municipality'], sz['street'], sz['number'], processed, len(stambene_zajednice)))
            continue
        if filter_municipality is not None and sz['municipality'] != filter_municipality:
            # logger.debug('Skipping {0}:{1}:{2}:{3} ({4}/{5}), filtered by municipality'.format(
            #     sz['district'], sz['municipality'], sz['street'], sz['number'], processed, len(stambene_zajednice)))
            continue

        if sz['processed']:
            logger.info('Skipping {0}:{1}:{2}:{3} ({4}/{5}), already '
                        'processed'.format(sz['district'], sz['municipality'], sz['street'], sz['number'], processed,
                                           len(stambene_zajednice)))
            continue
        logger.info('Processing {0}:{1}:{2}:{3} ({4}/{5})'.format(
            sz['district'], sz['municipality'], sz['street'], sz['number'], processed, len(stambene_zajednice)))
        process_stambena_zajednica_with_pbf(sz, all_addresses)
        # Save state after each iteration
        with open(progress_file, 'wb') as h:
            pickle.dump(stambene_zajednice, h, protocol=pickle.DEFAULT_PROTOCOL)
        # Dump current dataset to CSV once in a while
        if processed % 100 == 0:
            write_csv(output_file, stambene_zajednice)
    write_csv(output_file, stambene_zajednice)


if __name__ == '__main__':
    main()
