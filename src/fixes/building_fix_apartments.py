import csv
import logging
import logging.handlers
import osmapi
import time

def setup_logger(filename: str):
    """Simple logger used throughout whole scrapper - logs both to file and console."""
    logging_level = logging.INFO
    l = logging.getLogger()
    l.setLevel(logging_level)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    ch = logging.handlers.TimedRotatingFileHandler(filename=filename, when='midnight', interval=1)
    ch.setLevel(logging_level)
    ch.setFormatter(formatter)
    l.addHandler(ch)

    ch = logging.StreamHandler()
    ch.setLevel(logging_level)
    ch.setFormatter(formatter)
    l.addHandler(ch)

    return l


logger = setup_logger('fixes.log')
api = osmapi.OsmApi(passwordfile='osm-password',
                    changesetauto=True, changesetautosize=100, changesetautotags=
                    {u"comment": u"Serbian lint bot "
                                 "(https://gitlab.com/stalker314314/stambene-zajednice-analysis). "
                                 "Adding building=apartments on government provided open dataset",
                     u"tag": u"mechanical=yes"})


def load_results():
    stambene_zajednice = []
    with open('result.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            sz = {}
            sz['district'] = row['district']
            sz['municipality'] = row['municipality']
            sz['street'] = row['street']
            sz['number'] = row['number']
            sz['processed'] = True
            sz['node'] = [] if len(row['node']) <= 2 else [int(i) for i in row['node'][1:-1].split(',')]
            sz['way'] = [] if len(row['way']) <= 2 else [int(i) for i in row['way'][1:-1].split(',')]
            sz['relation'] = [] if len(row['relation']) <= 2 else \
                [int(i) for i in row['relation'][1:-1].split(',')]
            sz['multiple_entities_same_housenumber'] = row['multiple_entities_same_housenumber'] == 'True'
            sz['building_tag_present'] = row['building_tag_present'] == 'True'
            sz['building_is_apartments'] = row['building_is_apartments'] == 'True'
            stambene_zajednice.append(sz)
    return stambene_zajednice


def main():
    stambene_zajednice = load_results()
    count = len([sz for sz in stambene_zajednice
                 if sz['processed'] and len(sz['way']) > 0 and sz['building_tag_present'] and not sz['building_is_apartments']])
    i = 0
    for sz in stambene_zajednice:
        if sz['processed'] and len(sz['way']) > 0 and sz['building_tag_present'] and not sz['building_is_apartments']:
            i = i + 1
            if i < 0:  # If you need to skip already processed, set this to value greater than 0
                if i % 100 == 0:
                    logger.info('Progress: {0}/{1}'.format(i, count))
                continue
            logger.info('Progress: {0}/{1}'.format(i, count))
            for way_id in sz['way']:
                way = api.WayGet(way_id)
                if 'building' not in way['tag']:
                    logger.warning('{0} is missing building tag, skipping'.format(way_id))
                    continue
                if way['tag']['building'] == 'apartments':
                    logger.warning('{0} already have building=apartments tag, skipping'.format(way_id))
                    continue
                for k, v in way['tag'].items():
                    logger.info('{0}: {1}'.format(k, v))
                logger.info('https://www.openstreetmap.org/way/{0}'.format(way_id))
                logger.info('Ways found for this address: {0}'.format(len(sz['way'])))
                to_continue = input('Proceed (y/n):')
                if to_continue == '' or to_continue == 'y':
                    way['tag']['building'] = 'apartments'
                    api.WayUpdate(way)
    api.flush()


if __name__ == '__main__':
    main()