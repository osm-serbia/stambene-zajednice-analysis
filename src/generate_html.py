# -*- coding: utf-8 -*-

import argparse
import csv
from operator import itemgetter

from jinja2 import Environment, PackageLoader

from common import read_output_csv


def main():
    parser = argparse.ArgumentParser(
        description='Stambene zajednice HTML generation - helper tool to generate HTML report from output.csv file')
    parser.add_argument('-o', '--input-file', default='output.csv',
                        help='Name of input CSV file. Default value is "output.csv"')
    parser.add_argument('--filter-district', default=None, help='If filtering by district should be used')
    parser.add_argument('--filter-municipality', default=None, help='If filtering by municipality should be used')
    args = parser.parse_args()
    input_file = args.input_file
    filter_district = args.filter_district
    filter_municipality = args.filter_municipality

    env = Environment(loader=PackageLoader('__main__', '../templates'))
    template = env.get_template('index_template.html')

    districts = {'': 0}
    max_district_id = 0
    stambene_zajednice = read_output_csv(input_file, filter_district, filter_municipality)
    for sz in stambene_zajednice:
        if sz['district'] not in districts:
            max_district_id += 1
            districts[sz['district']] = max_district_id

        sz['district_id'] = districts[sz['district']]
    output = template.render(stambene_zajednice=stambene_zajednice, districts=districts)
    with open('index.html', 'w', encoding='utf-8') as fh:
        fh.write(output)


if __name__ == '__main__':
    main()