# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
from plotly import graph_objects as go


def has_elements(text):
    return len(np.fromstring(text[1:-1], sep=',', dtype=int)) > 0


def get_values(csvfile, district=None, municipality=None, format_v2=False):
    df = pd.read_csv(csvfile)
    if format_v2:
        has_elements_v = np.vectorize(has_elements)
        df['node'] = df['node'].apply(has_elements_v)
        df['way'] = df['way'].apply(has_elements_v)
        df['relation'] = df['relation'].apply(has_elements_v)

    processed = df[df.processed == True]
    if district:
        processed = processed[processed.district == district]
    if municipality:
        processed = processed[processed.municipality == municipality]

    if format_v2:
        processed = processed.assign(entity_found=(processed.node | processed.way | processed.relation))
    else:
        processed = processed.assign(entity_found=((~processed.node.isnull()) | (~processed.way.isnull())))
    entity_found = processed[processed.entity_found == True]
    if format_v2:
        have_way = entity_found[entity_found.way == True]
    else:
        have_way = entity_found[(~entity_found.way.isnull())]
    is_way_building = have_way[have_way.building_tag_present == True]
    is_building_apartments = is_way_building[is_way_building.building_is_apartments == True]

    return [len(processed), len(entity_found), len(have_way), len(is_way_building), len(is_building_apartments)]


def print_funnel_per_district():
    df = pd.read_csv('oktobar-result.csv')
    print(["", "In RGZ", "Found in OSM", "Tagged as ways", "Tagged as building", "Tagged as apartments"])
    for district in df.district.unique():
        x = get_values('oktobar-result.csv', district=district, format_v2=True)
        print(district, x)


def print_funnel_per_municipality():
    df = pd.read_csv('oktobar-result.csv')
    df = df[df.district == 'ГРАД БЕОГРАД']
    print(["", "In RGZ", "Found in OSM", "Tagged as ways", "Tagged as building", "Tagged as apartments"])
    for municipality in df.municipality.unique():
        x = get_values('oktobar-result.csv', district='ГРАД БЕОГРАД', municipality=municipality, format_v2=True)
        print(municipality, x)


def create_funnels():
    # avgust-result.csv can be found at https://stambenezajednice.z6.web.core.windows.net/avgust2019.csv
    x = get_values('avgust-result.csv', format_v2=False)
    fig = go.Figure(go.Funnel(
        x=x,
        y=["In RGZ", "Found in OSM", "Tagged as ways", "Tagged as building", "Tagged as apartments"],
        textinfo="label+value+percent initial",
        marker={"color": [
            "rgb(118,42,131)", "rgb(175,141,195)", "rgb(231,212,232)", "rgb(127,191,123)", "rgb(27,120,55)"]},
        textfont={"size": 18}
    ))
    fig.show()

    x = get_values('avgust-result.csv', district='ГРАД БЕОГРАД', format_v2=False)
    fig = go.Figure(go.Funnel(
        x=x,
        y=["In RGZ", "Found in OSM", "Tagged as ways", "Tagged as building", "Tagged as apartments"],
        textinfo="label+value+percent initial",
        marker={"color": [
            "rgb(118,42,131)", "rgb(175,141,195)", "rgb(231,212,232)", "rgb(127,191,123)", "rgb(27,120,55)"]},
        textfont={"size": 18}
    ))
    fig.show()

    # oktobar-result.csv can be found at https://stambenezajednice.z6.web.core.windows.net/oktobar2019.csv
    x = get_values('oktobar-result.csv', format_v2=True)
    fig = go.Figure(go.Funnel(
        x=x,
        y=["In RGZ", "Found in OSM", "Tagged as ways", "Tagged as building", "Tagged as apartments"],
        textinfo="label+value+percent initial",
        marker={"color": [
            "rgb(118,42,131)", "rgb(175,141,195)", "rgb(231,212,232)", "rgb(127,191,123)", "rgb(27,120,55)"]},
        textfont={"size": 18}
    ))
    fig.show()

    x = get_values('oktobar-result.csv', district='ГРАД БЕОГРАД', format_v2=True)
    fig = go.Figure(go.Funnel(
        x=x,
        y=["In RGZ", "Found in OSM", "Tagged as ways", "Tagged as building", "Tagged as apartments"],
        textinfo="label+value+percent initial",
        marker={"color": [
            "rgb(118,42,131)", "rgb(175,141,195)", "rgb(231,212,232)", "rgb(127,191,123)", "rgb(27,120,55)"]},
        textfont={"size": 18}
    ))
    fig.show()

    x = get_values('oktobar-result.csv', district='ГРАД БЕОГРАД', municipality='НОВИ БЕОГРАД', format_v2=True)
    fig = go.Figure(go.Funnel(
        x=x,
        y=["In RGZ", "Found in OSM", "Tagged as ways", "Tagged as building", "Tagged as apartments"],
        textinfo="label+value+percent initial",
        marker={"color": [
            "rgb(118,42,131)", "rgb(175,141,195)", "rgb(231,212,232)", "rgb(127,191,123)", "rgb(27,120,55)"]},
        textfont={"size": 18}
    ))
    fig.show()


if __name__ == '__main__':
    create_funnels()
    print_funnel_per_district()
    print_funnel_per_municipality()
